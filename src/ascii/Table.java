package ascii;

import java.util.LinkedList;
import java.util.List;

public class Table {
	private String headers[];
	private List<String[]> rows = new LinkedList<String[]>();
	private int cols[];

	public Table(int... cols) {
		this.cols = cols;
	}
	
	public void addHeader(String... headers) {
		this.headers = headers;
	}

	public void addRow(String... data) {
		rows.add(data);
	}

	public void print() {
		printHeader();

		for (int i = 0; i < rows.size(); i++) {
			printRow(rows.get(i));
			if (i + 1 < rows.size())
				printLine('+', '|');
		}

		printLine('-', '\'');
	}

	public void printHeader() {
		printLine('-', '.');
		printRow(headers);
		printLine('+', '|');
	}

	public void printRow(String... strings) {
		for (int i = 0; i < cols.length; i++) {
			if(strings[i].length() > cols[i])
				strings[i] = strings[i].substring(0, cols[i]);
			System.out.format("| %-" + cols[i] + "s ", strings[i]);
		}
		System.out.print("|\n");
	}

	public void printLine(char inside, char wrap) {
		System.out.print(wrap);
		for (int i = 0; i < cols.length; i++) {
			System.out.print(fill(cols[i] + 2, '-'));
			if (i + 1 < cols.length)
				System.out.print(inside);
		}
		System.out.format("%c\n", wrap);
	}

	public void printLine(char wrap) {
		printLine(wrap, wrap);
	}

	private int totalCol() {
		int sum = 0;
		for (int col : cols) {
			sum += col;
		}
		return sum;
	}

	private static String fill(int len, char c) {
		String str = "";
		for (int i = 0; i < len; i++) {
			str += c;
		}
		return str;
	}

}

