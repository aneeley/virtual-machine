package virtualmachine;

import java.io.File;
import java.util.Scanner;

import ascii.Table;

public class Launcher {
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		VirtualMachine vm = new VirtualMachine(scanner);

		File pwd = new File("data/");
		File[] files = pwd.listFiles();

		int col1Width = (int) Math.log10(files.length) + 1;
		int col2Width = 40 - col1Width;

		Table table = new Table(col1Width, col2Width);

		table.addHeader("#", "Command");

		for (int i = 0; i < files.length; i++)
			table.addRow(String.valueOf(i), files[i].getName());

		do {
			int choice;
			String input;

			table.print();

			do {
				do {
					System.out.print("\nChoose a number: ");
					input = scanner.nextLine();
				} while (!input.matches("-?(\\d)+"));

				choice = Integer.parseInt(input);

			} while (choice < 0 || choice >= files.length);

			File file = files[choice];

			System.out.format("Running %s...\n", file.getName());

			try {
				vm.process(file);
			} catch (Exception e) {
				vm = new VirtualMachine(scanner);
				System.out.println("\nAn error has occurred.");
				e.printStackTrace();
			}

			System.out.println("\nPress Enter to continue");

			scanner.nextLine();

		} while (true);
	}
}
