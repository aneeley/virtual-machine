package virtualmachine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class VirtualMachine {

	private int pc;
	private int[] memory;
	private boolean running;
	private Stack stack;
	private Scanner scanner;

	public VirtualMachine(Scanner scanner) {
		this.scanner = scanner;
	}

	public void process(File file) {
		init();
		loadFile(file);
		fetchDecodeExecute();
	}

	public boolean isRunning() {
		return running;
	}

	private void init() {
		pc = 0;
		memory = new int[512];
		stack = new Stack();
		running = true;
	}

	private void loadFile(File file) {
		HashMap<String, Integer> labelToAddress = new HashMap<String, Integer>();
		HashMap<Integer, String> addressToLabel = new HashMap<Integer, String>();

		int address = 0;
		int labelIndex = 0;

		String line;
		String label = "";

		BufferedReader br = null;

		try {
			br = new BufferedReader(new FileReader(file));

			while ((line = br.readLine()) != null) {

				line = line.trim();

				if (line.length() < 3)
					continue;

				String[] tokens = line.toUpperCase().split("\\s");
				String command = tokens.length > 0 ? tokens[0] : null;
				String arg = tokens.length > 1 ? tokens[1] : null;

				if (command == null)
					continue;

				if (command.charAt(command.length() - 1) == ':') {
					label = command.substring(0, command.length() - 1);
					continue;
				}

				if (Instruction.exists(command)) {

					Instruction instruction = Instruction.valueOf(command);

					if (!label.equals("")) {
						labelToAddress.put(label, address);
						label = "";
					}

					if (instruction == Instruction.STO && !arg.matches("\\d+")) {
						if (!labelToAddress.containsKey(arg)) {
							labelToAddress.put(arg, memory.length - 1 - labelIndex++);
						}
					}

					memory[address++] = instruction.opcode;

					if (instruction.argc > 0)
						if (arg.matches("-?\\d+"))
							memory[address++] = Integer.parseInt(arg);
						else
							if (labelToAddress.containsKey(arg))
								memory[address++] = labelToAddress.get(arg);
							else
								addressToLabel.put(address++, arg);
				}
			}
			for (int i = 0; i < memory.length; i++)
				if (addressToLabel.containsKey(i))
					memory[i] = labelToAddress.get(addressToLabel.get(i));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void fetchDecodeExecute() {
		int arg1, arg2;

		int readCount = 0;
		int writeCount = 0;

		while (running) {

			Instruction instruction = Instruction.getByOpcode(memory[pc++]);

			switch (instruction) {
			case INC:
				memory[memory[pc]] = memory[memory[pc++]] + 1;
				break;
			case DEC:
				memory[memory[pc]] = memory[memory[pc++]] - 1;
				break;
			case CONST:
				stack.push(memory[pc++]);
				break;
			case LOAD:
				stack.push(memory[memory[pc++]]);
				break;
			case STO:
				memory[memory[pc++]] = stack.pop();
				break;
			case ADD:
				stack.push(stack.pop() + stack.pop());
				break;
			case SUB:
				arg2 = stack.pop();
				arg1 = stack.pop();
				stack.push(arg1 - arg2);
				break;
			case MUL:
				stack.push(stack.pop() * stack.pop());
				break;
			case DIV:
				arg2 = stack.pop();
				arg1 = stack.pop();
				stack.push(arg1 - arg2);
				break;
			case EQL:
				arg2 = stack.pop();
				arg1 = stack.pop();
				if (arg1 == arg2)
					stack.push(1);
				else
					stack.push(0);
				break;
			case LSS:
				arg2 = stack.pop();
				arg1 = stack.pop();
				if (arg1 < arg2)
					stack.push(1);
				else
					stack.push(0);
				break;
			case GTR:
				arg2 = stack.pop();
				arg1 = stack.pop();
				if (arg1 > arg2)
					stack.push(1);
				else
					stack.push(0);
				break;
			case JMP:
				pc = memory[pc];
				break;
			case FJMP:
				if (stack.pop() == 0)
					pc = memory[pc];
				else
					pc++;
				break;
			case READ:
				readCount++;
				String input;
				do {
					System.out.format("#%d) Enter a number: ", readCount);
					input = scanner.nextLine();
					System.out.println();
				} while (!input.matches("-?(\\d)+"));
				stack.push(Integer.parseInt(input));
				break;
			case WRITE:
				writeCount++;
				System.out.format("#%d> %d\n", writeCount, stack.pop());
				break;
			case CALL:
				stack.push(pc + 1);
				pc = memory[pc];
				break;
			case RET:
				pc = stack.pop();
				break;
			case FCALL:
				if (stack.pop() == 0) {
					stack.push(pc + 1);
					pc = memory[pc];
				} else {
					pc++;
				}
				break;
			case HALT:
				running = false;
				break;
			}
		}
	}
}
