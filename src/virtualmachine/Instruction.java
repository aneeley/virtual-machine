package virtualmachine;

enum Instruction {
	HALT(0, 0), INC(6, 1), DEC(7, 1), CONST(8, 1), LOAD(9, 1), STO(10, 1), ADD(11, 0), SUB(12, 0), MUL(13, 0), DIV(14,
			0), EQL(15, 0), LSS(16, 0), GTR(17, 0), JMP(18, 1), FJMP(19, 1), READ(20, 0), WRITE(21, 0), CALL(22, 1), RET(
			23, 0), FCALL(24, 1);

	public static Instruction[] opcodes;

	static {
		opcodes = new Instruction[32];

		for (Instruction instruction : Instruction.values())
			opcodes[instruction.opcode] = instruction;
	}

	public static Instruction getByOpcode(int opcode) {
		return opcodes[opcode];
	}

	int opcode, argc;

	private Instruction(int opcode, int argc) {
		this.opcode = opcode;
		this.argc = argc;
	}

	public static boolean exists(String string) {
		for (Instruction instruction : Instruction.values())
			if (instruction.name().equals(string))
				return true;
		return false;
	}
}
