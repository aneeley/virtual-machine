package virtualmachine;

public class Stack {
	private Node sp;
	
	public int pop(){
		int data = sp.data;
		sp = sp.next;
		return data;
	}
	
	public void push(int data){
		Node node = new Node(data);
		node.next = sp;
		sp = node;
	}
	
	public int peek() {
		return sp.data;
	}
	
	public boolean empty() {
		return sp == null;
	}
	
	private class Node {
		Node next;
		int data;
		
		public Node(int data){
			this.data = data;
		}
	}
}
