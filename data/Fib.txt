input:
	read
	sto N
	
	load N
	const 1
	gtr
	fjmp input

const 2
sto I

const 0
sto F0

const 1
sto F1


fib:
	load F0 // f(n)
	load F1
	add
	sto F
	
	load F1 // f(n-1)
	sto F0
	
	load F // f(n-2)
	sto F1
	
	load I
	load N
	lss
	fjmp done // i >= n

	inc I

	jmp fib

done:
	load F
	write
	halt